from setuptools import setup

setup(
    name='cwapi',
    version='0.1a0',
    packages=['cityworks',],
    license='Creative Commons Attribution-Share Alike license',
    install_requires=['requests'],
)
