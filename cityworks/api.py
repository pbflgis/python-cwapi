import datetime
import json
import logging
import re
import requests

__all__ = (
    'API',
    'APIError',
)

_logger = logging.getLogger(__name__)

################################################################################

class APIError(RuntimeError):
    _errors = {
        -1: "Connection Error",
        0: "Ok",
        1: "Error",
        2: "Unauthorized",
        3: "Invalid Credentials",
    }

    def __init__(self, code, details=None):
        self.message = '[ERROR {}] {}:\n{}'.format(
            code, self._errors.get(code, 'Unknown Error'), '\n\n'.join(details or []))

    def __str__(self):
        return self.message

################################################################################

class _Endpoint(object):
    def __init__(self, connection, name):
        self.connection = connection
        self.name = name

    def __call__(self, *args, **kwargs):
        return self.connection(self.name, *args, **kwargs)

    def __getattr__(self, key):
        endpoint = _Endpoint(self.connection, self.name + '/' + key)
        setattr(self, key, endpoint)
        return endpoint

################################################################################

class JSONDecoder(json.JSONDecoder):
    _re_date = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    _re_time = '([0-9]{2}(:[0-9]{2}(:[0-9]{2}(\.[0-9]{3}([0-9]{3})?)?)?)?)?'\
               '([+-][0-9]{2}:[0-9]{2})?'
    _re_datetime = f'{_re_date}T{_re_time}'

    def object_hook(self, o):
        for k, v in tuple(o.items()):
            try:
                if re.match(f'^{self._re_datetime}$', v):
                    o[k] = datetime.datetime.fromisoformat(v)
                elif re.match(f'^{self._re_date}$', v):
                    o[k] = datetime.date.fromisoformat(v)
                elif re.match(f'^{self._re_time}$', v):
                    o[k] = datetime.time.fromisoformat(v)
            except TypeError:
                pass # probably "expected string or bytes-like object"
        return o

################################################################################

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        try:
            return o.isoformat()
        except AttributeError:
            return super().default(o)

################################################################################

class API(object):
    def __init__(self, url, *, token=None, **kwargs):
        if not url.endswith('/'):
            url += '/'

        self.url = url

        self.session = requests.Session()

        for k, v in kwargs.items():
            if hasattr(self.session, k):
                setattr(self.session, k, v)

    @property
    def token(self):
        return self.session.params.get('token')

    @token.setter
    def token(self, value):
        self.session.params.update(token=value)

    def __getattr__(self, key):
        endpoint = _Endpoint(self, key)
        setattr(self, key, endpoint)
        return endpoint

    def __call__(self, endpoint, *args, **kwargs):
        url = requests.compat.urljoin(self.url, 'services/' + endpoint)

        _logger.debug('Session Cookies: %r' % self.session.cookies.get_dict())
        _logger.debug('Request: %s', url)
        _logger.debug('Params: %r', kwargs)
        _logger.debug('Token: %r', self.token)

        params = { 'data': json.dumps(kwargs, cls=JSONEncoder) }
        response = self.session.post(url, data=params)

        try:
            result = response.json(cls=JSONDecoder)
        except json.JSONDecodeError:
            _logger.exception("expected JSON data, got:\n%r", response.content)
            raise

        _logger.info('=== %s %r %r', endpoint, args, kwargs)
        for msg in result.get('SuccessMessages', []):
            _logger.info('+++ %s', msg['DisplayText'])
            if msg['DebugDetails']:
                _logger.info('+++     %s', msg['DebugDetails'])
        for msg in result.get('WarningMessages', []):
            _logger.warning('>>> %s', msg['DisplayText'])
            if msg['DebugDetails']:
                _logger.warning('>>>     %s', msg['DebugDetails'])
        for msg in result.get('ErrorMessages', []):
            _logger.error('!!! %s', msg['DisplayText'])
            if msg['DebugDetails']:
                _logger.error('!!!     %s', msg['DebugDetails'])

        if result['Status'] != 0:
            details = [f"{m['DisplayText']}:\n{m['DebugDetails']}" for m in
                result.get('WarningMessages', []) +
                result.get('ErrorMessages', [])]
            raise APIError(result['Status'], details)

        _logger.debug('Result: %r', result)
        _logger.debug('Returned Cookies: %r', response.cookies.get_dict())

        return result.get('Value')
