import logging
import random
import string

__all__ = (
    'PasswordGenerator',
    'Geocoder',
    'WorkOrderTemplate',
    'InspectionTemplate',
    'RequestTemplate',
)

_logger = logging.getLogger(__name__)

################################################################################

class PasswordGenerator(object):
    UPPER = string.ascii_uppercase
    LOWER = string.ascii_lowercase
    DIGIT = string.digits
    PUNCT = string.punctuation
    CHARS = string.ascii_letters + string.digits #+ string.punctuation

    def __init__(self, api=None):
        prefs = {}

        if api is not None:
            prefs = {
                i.get('Element'): i.get('DefaultValue')
                    for i in api.Ams.Preferences.Global()
                    if i.get('Category') == 'PASSWORD'
                }

        self.min_chars = int(prefs.get('PASSWORD_MIN_LENGTH', 8))
        self.min_lower = int(prefs.get('PASSWORD_MIN_LOWER', 0))
        self.min_upper = int(prefs.get('PASSWORD_MIN_UPPER', 0))
        self.min_digit = int(prefs.get('PASSWORD_MIN_NUMERIC', 0))
        self.min_punct = int(prefs.get('PASSWORD_MIN_NONALPHANUMERIC', 0))

    def generate(self, length=None, seed=None):
        length = max(length or self.min_chars, self.min_chars)
        rng = random.Random(seed)

        min_chars = (length - self.min_lower - self.min_upper
                            - self.min_digit - self.min_punct)

        pw = [rng.choice(ch) for nb, ch in (
            (self.min_lower, self.LOWER),
            (self.min_upper, self.UPPER),
            (self.min_digit, self.DIGIT),
            (self.min_punct, self.PUNCT),
            (     min_chars, self.CHARS),
        ) for _ in range(nb)]

        _logger.debug('PasswordGenerator.generate: pw = %r', pw)

        rng.shuffle(pw)

        return ''.join(pw)

################################################################################

class Geocoder(object):
    def __init__(self, api, *, min_score=80, **defaults):
        self.api = api
        self.min_score = min_score
        self.defaults = defaults

    def geocode(self, address, min_score=None, **kwargs):
        params = self.defaults.copy()
        params.update(kwargs, Address=address)

        if min_score is None: min_score = self.min_score

        result = self.api.Gis.Geocode.LocationsFromAddress(**params)
        return sorted([x for x in result if x['Score'] >= min_score]
                    , key=lambda x: x['Score'], reverse=True)

    def reverse_geocode(self, X, Y, **kwargs):
        kwargs.update(X=X, Y=Y)
        return self.api.Gis.Geocode.AddressFromLocation(**kwargs)

################################################################################

def map_values(mapping, fields):
    _logger.debug('map_values(%r, %r) =>', mapping, fields)

    t = {k: map_values(v, fields) if type(v) is dict
            else eval(v[2:].lstrip(), {}, fields) if str(v).startswith('=>')
            else fields.get(v)
        for k, v in mapping.items()}

    _logger.debug('::: t = %r', t)

    return { k: v for k, v in t.items() if v is not None }

def map_keys(mapping, fields):
    _logger.debug('map_keys(%r, %r) =>', mapping, fields)
    result = { mapping[k]: v for k, v in fields.items() if k in mapping }
    _logger.debug('::: result = %r', result)
    return result

################################################################################

class TemplateBase(object):
    def __init__(self, api, **kwargs):
        self.api = api
        self.geocoder = Geocoder(api, **kwargs)

    def _map_fields(self, mapping=None, **fields):
        return fields if mapping is None else map_values(mapping, fields)

    def map_fields(self, mapping=None, **fields):
        fields = self._map_fields(mapping, **fields)

        address = fields.get('Address')
        x, y = fields.get('X'), fields.get('Y')

        if (address is not None) and (x is None or y is None):
            result = self.geocoder.geocode(address)

            _logger.debug('geocode result: %r', result)

            if result:
                fields.update({ k: v for k, v in result[0].items() if v })

        elif (address is None) and (x is not None and y is not None):
            result = self.geocoder.reverse_geocode(x, y)

            _logger.debug('reverse geocode result: %r', result)

            if result:
                fields.update({ k: v for k, v in result.items() if v })

        return fields

    def _create(self, **fields):
        raise NotImplementedError()

    def create(self, fields, mapping=None, **kwargs):
        fields = fields.copy()
        fields.update(kwargs)
        fields = self.map_fields(mapping, **fields)
        return self._create(**fields)

################################################################################

class WorkOrderTemplate(TemplateBase):
    def __init__(self, api, name=None, id=None, **kwargs):
        super().__init__(api, **kwargs)

        api = self.api

        if name is not None:
            result = api.Ams.WorkOrderTemplate.Search(Description=[name])
            if len(result) != 1:
                raise RuntimeError(f'template "{name}" is missing or ambiguous')
            id = result[0]

        if id is None:
            raise RuntimeError("'name' or 'id' is required")

        self.info = api.Ams.WorkOrderTemplate.ByIds(WOTemplateIds=[id])[0]

        self.custom_fields = api.Ams.WorkOrderTemplate.CustomFields(
            WOTemplateIds=[id]).pop(id)

        _logger.debug(self.custom_fields)

        self.custom_field_map = { v['CustFieldName']: v['CustFieldId']
                                  for v in self.custom_fields }

    def _map_fields(self, mapping=None, **fields):
        fields = super()._map_fields(mapping, **fields)

        if 'CustomFieldValues' in fields and self.custom_field_map:
            fields['CustomFieldValues'] = map_keys(
                self.custom_field_map, fields['CustomFieldValues'])

        return fields

    def _create(self, **fields):
        return self.api.Ams.WorkOrder.Create(
            EntityType=self.info['ApplyToEntity'],
            WOTemplateID=self.info['WOTemplateId'],
            **fields)

################################################################################

class InspectionTemplate(TemplateBase):
    def __init__(self, api, name=None, id=None, **kwargs):
        super().__init__(api, **kwargs)

        api = self.api

        if name is not None:
            result = api.Ams.InspectionTemplate.Search(InspTemplateName=[name])
            if len(result) != 1:
                raise RuntimeError(f'template "{name}" is missing or ambiguous')
            id = result[0]

        if id is None:
            raise RuntimeError("'name' or 'id' is required")

        self.info = api.Ams.InspectionTemplate.ByIds(InspTemplateIds=[id])

        self.qa = api.Ams.InspectionTemplate.QA(InspTemplateIds=[id])


################################################################################

class RequestTemplate(TemplateBase):
    def __init__(self, api, name=None, id=None, **kwargs):
        super().__init__(api, **kwargs)

        api = self.api

        if name is not None:
            result = api.Ams.ServiceRequestTemplate.Search(ProblemCode=[name])
            if len(result) != 1:
                raise RuntimeError(f'template "{name}" is missing or ambiguous')
            id = result[0]

        if id is None:
            raise RuntimeError("'name' or 'id' is required")

        self.info = api.Ams.ServiceRequestTemplate.ByIds(ProblemSids=[id])

        self.custom_fields = api.Ams.ServiceRequest.TemplateCustomFields(
            ProblemSid=id)

        self.custom_field_map = { v['CustFieldName']: v['CustFieldId']
                                  for v in self.custom_fields }

    def _map_fields(self, mapping=None, **fields):
        if 'CustomFieldValues' in fields and self.custom_field_map:
            fields['CustomFieldValues'] = map_keys(
                self.custom_field_map, fields['CustomFieldValues'])
        return super()._map_fields(mapping, **fields)

    def _create(self, **fields):
        return self.api.Ams.ServiceRequest.Create(
            ProblemSid=self.info['ProblemSid'],
            **fields)
