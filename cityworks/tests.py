import unittest
import configparser
from . import *

config = configparser.ConfigParser()
config.read(['test.conf'])

class PasswordGeneratorTest(unittest.TestCase):
    def test_passwordgenerator(self):
        pwg = PasswordGenerator()

        for _ in range(100):
            pw = pwg.generate()
            self.assertGreaterEqual(len(pw), pwg.min_chars)
            if pwg.min_digit: self.assertRegex(pw, '[0-9]')
            if pwg.min_upper: self.assertRegex(pw, '[A-Z]')
            if pwg.min_lower: self.assertRegex(pw, '[a-z]')

class CwGeneralTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cw = Connection(**config['cityworks'])

    def test_authentication_domains(self):
        domains = self.cw.api.General.Authentication.Domains()
        self.assertIsInstance(domains, list)
        self.assertNotEqual(domains, [])
        self.assertIsInstance(domains[0], dict)

    def test_authentication_user(self):
        user = self.cw.api.General.Authentication.User()
        self.assertIsInstance(user, dict)
        self.assertNotEqual(user, {})

# TODO create more class tests

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
