from .api import API
from .helpers import *

import logging

__all__ = (
    'Connection',
)

_logger = logging.getLogger(__name__)

class Connection(object):
    def __init__(self, url, username=None, password=None, *args, **kwargs):
        self.api = API(url, *args, **kwargs)

        if username:
            self.authenticate(username, password)

        self.geocoder = Geocoder(self.api)

    def authenticate(self, username, password):
        token = self.api.General.Authentication.Authenticate(
            LoginName=username, Password=password)['Token']

        if token:
            _logger.debug('CW auth token: %r', token)
            self.api.token = token

        return bool(token)

    def geocode(self, *args, **kwargs):
        return self.geocoder.geocode(*args, **kwargs)

    def reverse_geocode(self, *args, **kwargs):
        return self.geocoder.reverse_geocode(*args, **kwargs)

    def workorder_template(self, name=None, id=None, *args, **kwargs):
        return WorkOrderTemplate(self.api, name=name, id=id, *args, **kwargs)

    def inspection_template(self, name=None, id=None, *args, **kwargs):
        return InspectionTemplate(self.api, name=name, id=id, *args, **kwargs)

    def request_template(self, name=None, id=None, *args, **kwargs):
        return RequestTemplate(self.api, name=name, id=id, *args, **kwargs)
